package info.noip.ciprian.vector.multithreading;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

import info.noip.ciprian.vector.multithreading.exception.InvalidUsageThreadJobException;

/**
 * Abstract parent for ThreadJob classes. This is the actual job executed by
 * worker threads.
 * 
 * @author Marius Kleiner
 *
 */
public abstract class ThreadJob implements Comparable<ThreadJob> {

	// Determines the ID to find merge candidates. This id has to be unique over
	// all merge sets.
	protected Integer mergeSetId;
	// Map storing the amount of parts a merge set has.
	private static ConcurrentHashMap<Integer, Integer> mergeSetPartCount = new ConcurrentHashMap<Integer, Integer>();
	// ID for next merge set.
	private static Integer nextMergeSetId = 1;

	/**
	 * Constructor.
	 * 
	 * @throws InvalidUsageThreadJobException
	 */
	public ThreadJob() throws InvalidUsageThreadJobException {
		this(0);
	}

	/**
	 * Constructor for splitted jobs.
	 * 
	 * @param mergeSetId
	 *            ID of merge set.
	 * @throws InvalidUsageThreadJobException
	 */
	public ThreadJob(Integer mergeSetId) throws InvalidUsageThreadJobException {
		this.setMergeSetId(mergeSetId);
	}

	/**
	 * Execute the job an return the result.
	 * 
	 * @return Job result.
	 * @throws InvalidUsageThreadJobException
	 */
	protected abstract ThreadJobResult execute() throws InvalidUsageThreadJobException;

	/**
	 * Get an importance value influencing its execution order.
	 * 
	 * @return Importance value.
	 */
	protected abstract int getImportanceValue();

	/**
	 * Split this ThreadJob (if possible). Either return self in a list.
	 * ThreadJob implementations are about to decide by themselves if a split is
	 * possible, necessary or even has any advantage at all. In some cases
	 * splitting a job should be avoided because merging is too expensive. On
	 * the other hand the job can split to more job than requested (divide and
	 * conquer). The splitCount parameter is just a hint from ThreadPool.
	 * 
	 * @param splitCount
	 *            Amount of desired splits.
	 * @return List of job splits.
	 * @throws InvalidUsageThreadJobException
	 */
	protected ArrayList<ThreadJob> split(Integer splitCount) throws InvalidUsageThreadJobException {
		ArrayList<ThreadJob> returnList = new ArrayList<ThreadJob>();
		returnList.add(this);
		return returnList;
	}

	/**
	 * Set merge set ID.
	 * 
	 * @param mergeSetId
	 *            ID of merge set.
	 * @return this.
	 * @throws InvalidUsageThreadJobException
	 */
	ThreadJob setMergeSetId(Integer mergeSetId) throws InvalidUsageThreadJobException {
		if ((mergeSetId != 0) && (!ThreadJob.mergeSetPartCount.containsKey(mergeSetId))) {
			throw new InvalidUsageThreadJobException("Impossible to create merge job for non-existing merge set!");
		}
		this.mergeSetId = mergeSetId;
		return this;
	}

	/**
	 * Comparable method.
	 */
	public final int compareTo(ThreadJob compareJob) {
		return this.getImportanceValue() - compareJob.getImportanceValue();
	}

	/**
	 * Get next merge set ID.
	 * 
	 * @param partCount
	 *            Amount of parts for created set.
	 * @return Set ID.
	 */
	protected Integer getNextMergeSetId(Integer partCount) {
		Integer id = ThreadJob.nextMergeSetId++;
		ThreadJob.mergeSetPartCount.put(id, partCount);
		return id;
	}

	/**
	 * Get amount of parts the according merge set has.
	 * 
	 * @param mergeSetId
	 *            ID identifying merge set.
	 * @return Amount of parts or null if set does not exist.
	 */
	protected static Integer getMergeSetPartCount(Integer mergeSetId) {
		return ThreadJob.mergeSetPartCount.get(mergeSetId);
	}

	/**
	 * Decrement merge set part counter.
	 * 
	 * @param mergeSetId
	 *            ID identifying merge set.
	 * @throws InvalidUsageThreadJobException
	 */
	static void decrementMergeSetPartCount(Integer mergeSetId) throws InvalidUsageThreadJobException {
		Integer mergeSetPartCount = ThreadJob.mergeSetPartCount.get(mergeSetId);
		if (mergeSetPartCount == null) {
			throw new InvalidUsageThreadJobException("Impossible to decrement non-existent merge set id counter!");
		}
		if (mergeSetPartCount <= 0) {
			throw new InvalidUsageThreadJobException("Impossible to decrement set id counter beyond zero!");
		}
		ThreadJob.mergeSetPartCount.put(mergeSetId, mergeSetPartCount - 1);
	}

}
