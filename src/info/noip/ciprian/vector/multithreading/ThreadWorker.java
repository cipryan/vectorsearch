package info.noip.ciprian.vector.multithreading;

import java.util.logging.Level;
import java.util.logging.Logger;

import info.noip.ciprian.vector.multithreading.ThreadPoolJob.Status;
import info.noip.ciprian.vector.multithreading.exception.MultithreadingException;

/**
 * This Runnable represents the actual worker thread executing TreadJobs. When
 * notified by ThreadPoolJob it tries to acquire a job from the available queue.
 * 
 * @author Marius Kleiner
 *
 */
class ThreadWorker implements Runnable {

	// Thread pool job
	private ThreadPoolJob<? extends ThreadJobResult> threadPoolJob;
	// Thread job
	private ThreadJob threadJob;
	// Thread number
	private static Integer threadNumberCounter = 1;
	private Integer threadNumber;

	/**
	 * Constructor.
	 * 
	 * @param threadPoolJob
	 *            Thread pool job.
	 * 
	 */
	ThreadWorker(ThreadPoolJob<? extends ThreadJobResult> threadPoolJob) {
		this.threadPoolJob = threadPoolJob;
		this.threadNumber = ThreadWorker.threadNumberCounter++;
	}

	/**
	 * Get thread job.
	 * 
	 * @return Thread job.
	 */
	ThreadJob getThreadJob() {
		return this.threadJob;
	}

	/**
	 * Get thread number.
	 * 
	 * @return Thread number.
	 */
	int getThreadNumber() {
		return this.threadNumber;
	}

	@Override
	public void run() {
		Logger.getLogger(ThreadPool.class.getName()).log(Level.FINE,
				ThreadWorker.class.getSimpleName() + "(" + this.getThreadNumber() + "): Start");
		boolean initialRun = true;
		runningloop: while (true) {
			try {
				if (!initialRun) {
					Logger.getLogger(ThreadPool.class.getName()).log(Level.FINE,
							ThreadWorker.class.getSimpleName() + "(" + this.getThreadNumber() + "): going to sleep");
					synchronized (this.threadPoolJob) {
						if (this.threadPoolJob.isFinished()) {
							break runningloop;
						}
						this.threadPoolJob.wait();
					}
					Logger.getLogger(ThreadPool.class.getName()).log(Level.FINE,
							ThreadWorker.class.getSimpleName() + "(" + this.getThreadNumber() + "): waking up");
				} else {
					initialRun = false;
				}
				boolean hasMore = true;
				while (hasMore) {
					this.threadJob = this.threadPoolJob.getJob();
					if (this.threadJob == null) {
						this.threadPoolJob.workerGoingToSleep();
						if (this.threadPoolJob.isFinished()) {
							break runningloop;
						}
						break;
					}
					Logger.getLogger(ThreadPool.class.getName()).log(Level.FINE,
							ThreadWorker.class.getSimpleName() + "(" + this.getThreadNumber() + "): Executing job "
									+ this.threadJob.getClass().getSimpleName());
					ThreadJobResult jobResult = this.threadJob.execute();
					Logger.getLogger(ThreadPool.class.getName()).log(Level.FINE,
							ThreadWorker.class.getSimpleName() + "(" + this.getThreadNumber() + "): Finished job "
									+ this.threadJob.getClass().getSimpleName());
					this.threadJob = null;
					Status status = this.threadPoolJob.returnJobResult(jobResult);
					switch (status) {
					case FINISHED:
						break runningloop;
					case HAS_JOBS_ATM:
						hasMore = true;
						break;
					case NO_JOBS_ATM:
						hasMore = false;
						break;
					}
				}
			} catch (InterruptedException e) {
				this.threadPoolJob.notifyThreadFailure(this, e);
			} catch (MultithreadingException e) {
				this.threadPoolJob.notifyThreadFailure(this, e);
			}
		}
		Logger.getLogger(ThreadPool.class.getName()).log(Level.FINE,
				ThreadWorker.class.getSimpleName() + "(" + this.getThreadNumber() + "): Terminating");
	}
}
