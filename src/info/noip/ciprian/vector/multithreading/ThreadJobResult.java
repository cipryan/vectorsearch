package info.noip.ciprian.vector.multithreading;

import java.util.ArrayList;

import info.noip.ciprian.vector.multithreading.exception.InvalidUsageThreadJobException;

/**
 * Result of single thread jobs. Must implement split().
 * 
 * @author Marius Kleiner
 *
 */
public abstract class ThreadJobResult {

	// Merge set ID.
	private Integer mergeSetId = 0;

	/**
	 * Progress this job result and return it the the pool job in the right way.
	 * 
	 * @param threadPoolJob
	 * @throws InvalidUsageThreadJobException
	 */
	protected abstract void progressResult(ThreadPoolJob<? extends ThreadJobResult> threadPoolJob)
			throws InvalidUsageThreadJobException;

	/**
	 * Merge job results.
	 * 
	 * @param secondMergeResult
	 *            Second result to merge with.
	 * @return Merged job result.
	 * @throws InvalidUsageThreadJobException
	 */
	protected abstract ThreadJobResult merge(ThreadJobResult secondMergeResult) throws InvalidUsageThreadJobException;

	/**
	 * Add new job to list.
	 * 
	 * @param threadJob
	 *            New thread job to add.
	 * @param threadPoolJob
	 *            Parent pool job.
	 */
	protected final void addNewJob(ThreadJob threadJob, ThreadPoolJob<? extends ThreadJobResult> threadPoolJob) {
		threadPoolJob.addJobToList(threadJob);
	}

	/**
	 * Add new jobs to list.
	 * 
	 * @param threadJobs
	 *            New thread jobs to add.
	 * @param threadPoolJob
	 *            Parent pool job.
	 */
	protected final void addNewJobs(ArrayList<ThreadJob> threadJobs,
			ThreadPoolJob<? extends ThreadJobResult> threadPoolJob) {
		threadPoolJob.addJobsToList(threadJobs);
	}

	/**
	 * Add this result to merge set.
	 * 
	 * @param mergeSetId
	 *            Identifying merge set ID.
	 * @param threadPoolJob
	 *            Parent pool job.
	 */
	protected final void addToMergeSet(Integer mergeSetId, ThreadPoolJob<? extends ThreadJobResult> threadPoolJob) {
		threadPoolJob.addResultToMergeSet(mergeSetId, this);
	}

	/**
	 * Add this result to final results.
	 * 
	 * @param threadPoolJob
	 *            Parent pool job.
	 */
	protected final void addToFinalResults(ThreadPoolJob<? extends ThreadJobResult> threadPoolJob) {
		threadPoolJob.addResultToFinalResults(this);
	}

	/**
	 * Set if this result requires merge.
	 * 
	 * @param requiresMerge
	 *            New value for merge requirement.
	 * @return this.
	 */
	public final ThreadJobResult setRequiresMerge(Integer mergeSetId) {
		this.mergeSetId = mergeSetId;
		return this;
	}

	/**
	 * Set this result requires no merge.
	 * 
	 * @return this.
	 */
	public final ThreadJobResult setNoMergeRequired() {
		return this.setRequiresMerge(0);
	}

	/**
	 * Get merge set ID.
	 * 
	 * @return Merge set ID.
	 */
	public final Integer getMergeSetId() {
		return this.mergeSetId;
	}

	/**
	 * Check if merge is required.
	 * 
	 * @return the requiresMerge.
	 */
	public final boolean requiresMerge() {
		return (this.mergeSetId != 0);
	}

}
