package info.noip.ciprian.vector.multithreading;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import info.noip.ciprian.vector.multithreading.exception.InvalidUsageThreadJobException;

/**
 * Parent job of ThreadPool. Manages all ThreadJobs and their split and merge
 * processes.
 * 
 * @author MariusKleiner
 *
 */
public class ThreadPoolJob<Result extends ThreadJobResult> {

	// ThreadPoolJobStatus
	enum Status {
		NO_JOBS_ATM, HAS_JOBS_ATM, FINISHED
	}

	// Amount of running jobs
	private AtomicInteger runningJobs = new AtomicInteger(0);
	// Finishes state
	private AtomicBoolean finished = new AtomicBoolean(false);
	// Thread Pool this job is bound to.
	private ThreadPool<? extends ThreadJobResult> threadPool = null;
	// Result list
	private ArrayList<ThreadJobResult> resultList = new ArrayList<ThreadJobResult>();
	// List of jobs
	private ArrayList<ThreadJob> jobList = new ArrayList<ThreadJob>();
	// Merge sets
	private ConcurrentHashMap<Integer, ArrayList<ThreadJobResult>> mergeSets = new ConcurrentHashMap<Integer, ArrayList<ThreadJobResult>>();

	/**
	 * Constructor.
	 * 
	 * @param initialJobList
	 *            List of initial jobs.
	 */
	ThreadPoolJob(ArrayList<ThreadJob> initialJobList) {
		this.addJobsToList(initialJobList);
	}

	/**
	 * Bind this job to a pool.
	 * 
	 * @param threadPool
	 */
	void bindToPool(ThreadPool<? extends ThreadJobResult> threadPool) {
		this.threadPool = threadPool;
	}

	/**
	 * Get next job to execute.
	 * 
	 * @return ThreadJob to execute.
	 * @throws InvalidUsageThreadJobException
	 */
	synchronized ThreadJob getJob() throws InvalidUsageThreadJobException {
		this.runningJobs.incrementAndGet();
		// Check if any job exists
		if (this.jobList.isEmpty()) {
			return null;
		}
		// Determine desired amount of splits
		int idleWorkers = (this.threadPool.getTotalWorkerCount() - this.runningJobs.get()) + 1;
		int totalAvailableJobs = this.jobList.size();
		ThreadJob splitJobCandidate = this.jobList.remove(0);
		ArrayList<ThreadJob> splitJobList = splitJobCandidate
				.split(this.getDesiredSplitAmount(idleWorkers, totalAvailableJobs));
		if (splitJobList.size() == 1) {
			return splitJobList.remove(0);
		} else {
			this.addJobsToList(splitJobList);
			return this.jobList.remove(0);
		}
	}

	/**
	 * Calculate amount of desired parts for splitted job.
	 * 
	 * @param idleWorkers
	 *            Amount of idle workers.
	 * 
	 * @param totalAvailableJobs
	 *            Total amount of available jobs in queue.
	 * 
	 * @return Amount of desired split job parts.
	 */
	private Integer getDesiredSplitAmount(int idleWorkers, int totalAvailableJobs) {
		int splitAmount = (int) Math.ceil(((double) idleWorkers / (double) totalAvailableJobs));
		return splitAmount;
	}

	/**
	 * Check if merge jobs can be constructed.
	 * 
	 * @throws InvalidUsageThreadJobException
	 */
	synchronized private void constructMergeJobs() throws InvalidUsageThreadJobException {
		if (!this.mergeSets.isEmpty()) {
			for (Iterator<Entry<Integer, ArrayList<ThreadJobResult>>> it = this.mergeSets.entrySet().iterator(); it
					.hasNext();) {
				Entry<Integer, ArrayList<ThreadJobResult>> entry = it.next();
				Integer mergeSetPartCount = ThreadJob.getMergeSetPartCount(entry.getKey());
				if (mergeSetPartCount == null) {
					throw new InvalidUsageThreadJobException(
							"Impossible to progress merge job for non-existing merge set!");
				}
				ArrayList<ThreadJobResult> mergeJobResults = entry.getValue();
				if ((mergeJobResults.size() == 1) && (mergeSetPartCount == 1)) {
					mergeJobResults.remove(0).setNoMergeRequired().progressResult(this);
					ThreadJob.decrementMergeSetPartCount(entry.getKey());
					it.remove();
				} else if (mergeJobResults.size() >= 2) {
					MergeThreadJob mergeJob = new MergeThreadJob(mergeJobResults.remove(0), mergeJobResults.remove(0));
					this.addJobToList(mergeJob);
					ThreadJob.decrementMergeSetPartCount(entry.getKey());
				}
			}
		}
	}

	/**
	 * This method takes the resulting jobs returned by a finished job and
	 * returns if there are more jobs in the queue atm.
	 * 
	 * @param jobResult
	 *            The result of an executed job.
	 * @return Current job status.
	 * @throws InvalidUsageThreadJobException
	 */
	synchronized Status returnJobResult(ThreadJobResult jobResult) throws InvalidUsageThreadJobException {
		jobResult.progressResult(this);
		this.constructMergeJobs();
		this.runningJobs.decrementAndGet();
		if (this.jobList.isEmpty() && this.runningJobs.get() == 0 && !this.finished.get()) {
			this.finished.set(true);
			this.threadPool.notifyFinished();
		}
		this.notifyAll();
		if (this.finished.get()) {
			return Status.FINISHED;
		} else if (this.jobList.isEmpty()) {
			return Status.NO_JOBS_ATM;
		} else {
			return Status.HAS_JOBS_ATM;
		}
	}

	/**
	 * Treat another worker as sleeping.
	 */
	synchronized void workerGoingToSleep() {
		this.runningJobs.decrementAndGet();
	}

	/**
	 * Add result to merge set.
	 * 
	 * @param mergeSet
	 *            Merge set ID.
	 * @param result
	 *            Result.
	 */
	synchronized void addResultToMergeSet(Integer mergeSet, ThreadJobResult result) {
		if (!this.mergeSets.containsKey(mergeSet)) {
			this.mergeSets.put(mergeSet, new ArrayList<ThreadJobResult>());
		}
		this.mergeSets.get(mergeSet).add(result);
	}

	/**
	 * Add result to final results.
	 * 
	 * @param result
	 *            Final result.
	 */
	synchronized void addResultToFinalResults(ThreadJobResult result) {
		this.resultList.add(result);
	}

	/**
	 * Add job to job list.
	 * 
	 * @param threadJob
	 *            Job to add.
	 */
	synchronized void addJobToList(ThreadJob threadJob) {
		this.jobList.add(threadJob);
		this.jobList.sort(Collections.reverseOrder());
	}

	/**
	 * Add job to job list.
	 * 
	 * @param threadJob
	 *            Job to add.
	 */
	synchronized void addJobsToList(ArrayList<ThreadJob> threadJobs) {
		for (ThreadJob threadJob : threadJobs) {
			this.jobList.add(threadJob);
		}
		this.jobList.sort(Collections.reverseOrder());
	}

	/**
	 * Notify thread failure.
	 * 
	 * @param threadWorker
	 *            Failed thread worker.
	 * @param e
	 *            Thread exception.
	 */
	synchronized void notifyThreadFailure(ThreadWorker threadWorker, Exception e) {
		this.threadPool.notifyThreadFailure(threadWorker, e);
	}

	/**
	 * Check if job is finished.
	 * 
	 * @return If job is finished.
	 */
	boolean isFinished() {
		return this.finished.get();
	}

	/**
	 * Get list of results.
	 * 
	 * @return
	 */
	ArrayList<ThreadJobResult> getResultList() {
		return this.resultList;
	}

}
