package info.noip.ciprian.vector.multithreading;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.IntStream;

import info.noip.ciprian.vector.multithreading.exception.InvalidUsageThreadPoolException;

/**
 * This class represent the thread pool which can take and execute a list of
 * thread jobs.
 * 
 * @author Marius Kleiner
 *
 */
public class ThreadPool<Result extends ThreadJobResult> {

	// Flag if job is finished
	private AtomicBoolean finished = new AtomicBoolean(false);
	// Flag if job is running.
	private AtomicBoolean running = new AtomicBoolean(false);
	// List of threads in this pool.
	private ArrayList<ThreadWorker> workerList = new ArrayList<ThreadWorker>();
	// Total pool job.
	private ThreadPoolJob<Result> threadPoolJob;
	// Registered listeners.
	private ArrayList<ThreadPoolListener> listeners = new ArrayList<ThreadPoolListener>();
	// Flag if auto recover from failure
	private boolean autoRecoverFromFailure = false;
	// Maximum amount of thread recoveries
	private int maxAutoRecoveries = 0;
	// Amount of thread recoveries
	private int autoRecoveries = 0;

	/**
	 * Standard constructor generating a pool of threads matching systems core
	 * count.
	 * 
	 * @param initialJobList
	 *            List of initial jobs.
	 * @throws InvalidUsageThreadPoolException
	 */
	public ThreadPool(ArrayList<ThreadJob> initialJobList) throws InvalidUsageThreadPoolException {
		this(initialJobList, Runtime.getRuntime().availableProcessors());
	}

	/**
	 * Constructor.
	 * 
	 * @param initialJobList
	 *            List of initial jobs.
	 * @param threadAmount
	 *            Number of threads to spawn in pool.
	 * @throws InvalidUsageThreadPoolException
	 */
	public ThreadPool(ArrayList<ThreadJob> initialJobList, int threadAmount) throws InvalidUsageThreadPoolException {
		if (threadAmount <= 1) {
			throw new InvalidUsageThreadPoolException("It is impossible to start less than 1 thread(s)!");
		}
		Logger.getLogger(ThreadPool.class.getName()).log(Level.FINE,
				ThreadPool.class.getSimpleName() + ": Initializing ThreadPool with " + threadAmount + " worker threads");
		this.threadPoolJob = new ThreadPoolJob<Result>(initialJobList);
		this.threadPoolJob.bindToPool(this);
		IntStream.range(0, threadAmount).forEach(i -> spawnThread(this.threadPoolJob));
	}

	/**
	 * Spawn a new thread and add to pool list.
	 * 
	 * @param poolJob
	 *            Total pool job.
	 * @return Created worker thread.
	 */
	private ThreadWorker spawnThread(ThreadPoolJob<Result> poolJob) {
		ThreadWorker workerThread = new ThreadWorker(poolJob);
		this.workerList.add(workerThread);
		return workerThread;
	}

	/**
	 * Start threads and let them start their work until all is done.
	 * 
	 * @throws InvalidUsageThreadPoolException
	 */
	public void execute() throws InvalidUsageThreadPoolException {
		if (this.running.get()) {
			throw new InvalidUsageThreadPoolException("Impossible to execute a running thread pool twice!");
		}
		for (ThreadWorker worker : this.workerList) {
			Thread workerThread = new Thread(worker);
			workerThread.start();
		}
		Logger.getLogger(ThreadPool.class.getName()).log(Level.FINE,
				ThreadPool.class.getSimpleName() + ": Start execution of Job");
		for (ThreadPoolListener listener : this.listeners) {
			listener.notifyStarted();
		}
		this.running.set(true);
	}

	/**
	 * Get all initial job results.
	 * 
	 * @return List of job results.
	 * @throws InvalidUsageThreadPoolException
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<Result> getResults() throws InvalidUsageThreadPoolException {
		if (!this.finished.get()) {
			throw new InvalidUsageThreadPoolException("Impossible to get result of running jobs!");
		}
		try {
			return ((ArrayList<Result>) this.threadPoolJob.getResultList());
		} catch (java.lang.ClassCastException e) {
			throw new InvalidUsageThreadPoolException(
					"Wrong implementation of ThreadJob and according ThreadJobResult classes!");
		}
	}

	/**
	 * Get notified that ThreadPoolJob is finished.
	 * 
	 * @param success
	 *            If execution finished successfully.
	 */
	void notifyFinished() {
		this.finished.set(true);
		Logger.getLogger(ThreadPool.class.getName()).log(Level.FINE,
				ThreadPool.class.getSimpleName() + ": ThreadPoolJob finished - notifying listeners");
		for (ThreadPoolListener listener : this.listeners) {
			listener.notifyFinished();
		}
		// Notify all threads for the last time to ensure they all terminate
		this.threadPoolJob.notifyAll();
	}

	/**
	 * Notify thread failure.
	 * 
	 * @param threadWorker
	 *            Failed thread number.
	 * @param e
	 *            Exception.
	 */
	synchronized void notifyThreadFailure(ThreadWorker threadWorker, Exception e) {
		Iterator<ThreadWorker> it = this.workerList.iterator();
		while (it.hasNext()) {
			ThreadWorker worker = it.next();
			if (worker.equals(threadWorker)) {
				it.remove();
			}
		}
		// Notify thread failure
		Logger.getLogger(ThreadPool.class.getName()).log(Level.FINE,
				ThreadPool.class.getSimpleName() + ": ThreadWorker failed (" + this.getTotalWorkerCount() + " left)");
		for (ThreadPoolListener listener : this.listeners) {
			listener.notifyThreadFailure(threadWorker.getThreadNumber(), e);
		}
		// Recover from thread failure
		if (this.autoRecoverFromFailure) {
			this.recoverFromFailure(threadWorker);
		}
		// Total failure
		if (this.workerList.isEmpty()) {
			Logger.getLogger(ThreadPool.class.getName()).log(Level.FINE,
					ThreadPool.class.getSimpleName() + ": No ThreadWorker instance left - aborting execution");
			for (ThreadPoolListener listener : this.listeners) {
				listener.notifyJobFailure();
			}
		}
	}

	/**
	 * Recover from failure.
	 */
	private void recoverFromFailure(ThreadWorker threadWorker) {
		if ((this.maxAutoRecoveries == 0) || (this.autoRecoveries < this.maxAutoRecoveries)) {
			this.autoRecoveries++;
			// Recover job
			if (threadWorker.getThreadJob() != null) {
				this.threadPoolJob.addJobToList(threadWorker.getThreadJob());
			}
			// Restart thread
			ThreadWorker worker = this.spawnThread(this.threadPoolJob);
			Thread workerThread = new Thread(worker);
			workerThread.start();
		}
	}

	/**
	 * Register a thread pool listener.
	 * 
	 * @param listener
	 *            Instance implementing ThreadPoolListener interface.
	 */
	public void addThreadPoolListener(ThreadPoolListener listener) {
		this.listeners.add(listener);
	}

	/**
	 * Get total amount of workers, if they are busy or not.
	 * 
	 * @return Amount of worker threads.
	 */
	int getTotalWorkerCount() {
		return this.workerList.size();
	}
	
	/**
	 * Enable auto recovery.
	 */
	public void enableAutoRecovery() {
		this.autoRecoverFromFailure = true;
	}
	
	/**
	 * Disable auto recovery.
	 */
	public void disabloeAutoRecovery() {
		this.autoRecoverFromFailure = false;
	}

	/**
	 * Set maximum amount of allowed auto recoveries.
	 * 
	 * @param newValue
	 *            New value.
	 */
	public void setMaxAutoRecoveries(int newValue) {
		this.maxAutoRecoveries = newValue;
	}
}
