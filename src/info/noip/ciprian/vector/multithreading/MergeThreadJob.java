package info.noip.ciprian.vector.multithreading;

import info.noip.ciprian.vector.multithreading.exception.InvalidUsageThreadJobException;

/**
 * This job executes the merge method of ThreadJobResult instances.
 * 
 * @author Marius Kleiner
 *
 */
class MergeThreadJob extends ThreadJob {

	// Merge job results
	private ThreadJobResult firstMergeResult;
	private ThreadJobResult secondMergeResult;

	/**
	 * Constructor.
	 * 
	 * @param firstMergeResult
	 *            First result to merge.
	 * @param secondMergeResult
	 *            Second result to merge.
	 * @throws InvalidUsageThreadJobException
	 */
	MergeThreadJob(ThreadJobResult firstMergeResult, ThreadJobResult secondMergeResult)
			throws InvalidUsageThreadJobException {
		super();
		this.firstMergeResult = firstMergeResult;
		this.secondMergeResult = secondMergeResult;
	}

	@Override
	protected ThreadJobResult execute() throws InvalidUsageThreadJobException {
		ThreadJobResult mergeResult = this.firstMergeResult.merge(this.secondMergeResult);
		return mergeResult;
	}

	@Override
	protected int getImportanceValue() {
		// Always execute merges first
		return Integer.MAX_VALUE;
	}

}
