package info.noip.ciprian.vector.multithreading;

/**
 * Listener for ThreadPool execution.
 * 
 * @author Marius Kleiner
 *
 */
public interface ThreadPoolListener {

	/**
	 * Notify that thread pool has started progressing its current job.
	 */
	public void notifyStarted();

	/**
	 * Notify that thread pool has finished its current job.
	 */
	public void notifyFinished();

	/**
	 * Notify thread failure.
	 * 
	 * @param threadNumber
	 *            Thread number.
	 * @param e
	 *            Thread exception.
	 */
	public void notifyThreadFailure(int threadNumber, Exception e);

	/**
	 * Notify job failed.
	 */
	public void notifyJobFailure();

}
