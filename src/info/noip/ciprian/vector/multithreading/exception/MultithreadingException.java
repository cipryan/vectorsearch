package info.noip.ciprian.vector.multithreading.exception;

/**
 * Multithreading base exception.
 * 
 * @author Marius Kleiner
 *
 */
@SuppressWarnings("serial")
public class MultithreadingException extends Exception {

	// Error message
	protected String errorMessage;

	/**
	 * Constructor.
	 * 
	 * @param errorMessage
	 *            Error message text.
	 */
	public MultithreadingException(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}