package info.noip.ciprian.vector.multithreading.exception;

@SuppressWarnings("serial")
public class InvalidUsageThreadJobException extends MultithreadingException {

	public InvalidUsageThreadJobException(String errorMessage) {
		super(errorMessage);
	}

}
