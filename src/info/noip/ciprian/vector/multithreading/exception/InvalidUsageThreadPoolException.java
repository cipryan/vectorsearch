package info.noip.ciprian.vector.multithreading.exception;

@SuppressWarnings("serial")
public class InvalidUsageThreadPoolException extends MultithreadingException {

	public InvalidUsageThreadPoolException(String errorMessage) {
		super(errorMessage);
	}

}
