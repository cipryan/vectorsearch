package info.noip.ciprian.vector.trie;

import java.util.ArrayList;
import java.util.HashMap;

import info.noip.ciprian.vector.trie.exception.PrefixTrieNodeException;

/**
 * Prefix trie node containing all directly matching items, all child items and
 * all child nodes.
 * 
 * @author Marius Kleiner
 *
 */
public class PrefixTrieNode {

	// Child nodes
	protected HashMap<Character, PrefixTrieNode> childs = new HashMap<Character, PrefixTrieNode>();
	// Directly matching item
	protected String item;
	// All matching items (including childs)
	protected ArrayList<String> allItems;
	// Alphabet of possible item elements
	protected ArrayList<Character> alphabet;
	// Node count
	protected static Integer nodeCount = 0;

	/**
	 * Constructor.
	 * 
	 * @param trie
	 *            Trie this node belongs to.
	 */
	public PrefixTrieNode(ArrayList<Character> alphabet) {
		this(alphabet, new ArrayList<String>());
	}

	/**
	 * Constructor.
	 * 
	 * @param trie
	 *            Trie this node belongs to.
	 * 
	 * @param allItems
	 *            All matching items.
	 */
	public PrefixTrieNode(ArrayList<Character> alphabet, ArrayList<String> allItems) {
		this.alphabet = alphabet;
		this.allItems = allItems;
	}

	/**
	 * Append child to parent and return child.
	 * 
	 * @param childKey
	 *            Key identifying child node.
	 * @param allItems
	 *            All matching items.
	 * @return Appended child node.
	 * @throws PrefixTrieNodeException
	 */
	public PrefixTrieNode appendChild(Character childKey, ArrayList<String> allItems) throws PrefixTrieNodeException {
		return this.appendChild(childKey, allItems, null);
	}

	/**
	 * Append child to parent and return child.
	 * 
	 * @param childKey
	 *            Key identifying child node.
	 * @param allItems
	 *            All matching items.
	 * @param directMatchItem
	 *            Direct match.
	 * @return Appended child node.
	 * @throws PrefixTrieNodeException
	 */
	public PrefixTrieNode appendChild(Character childKey, ArrayList<String> allItems, String directMatchItem)
			throws PrefixTrieNodeException {
		PrefixTrieNode childNode = new PrefixTrieNode(this.alphabet, allItems);
		childNode.setDirectMatchItem(directMatchItem);
		this.addChildNode(childKey, childNode);
		PrefixTrieNode.nodeCount++;
		return childNode;
	}

	/**
	 * Get all items.
	 * 
	 * @return List of all items.
	 */
	public ArrayList<String> getItems() {
		return this.allItems;
	}

	/**
	 * Check if node contains a directly matching item.
	 * 
	 * @return If has direct match.
	 */
	public boolean hasDirectMatchItem() {
		return (this.item != null);
	}

	/**
	 * Get direct match item.
	 * 
	 * @return Direct match item.
	 */
	public String getDirectMatchItem() {
		return this.item;
	}

	/**
	 * Set direct match item.
	 * 
	 * @param item
	 *            New direct match item.
	 */
	public void setDirectMatchItem(String item) {
		if (item == null) {
			return;
		}
		this.item = item;
	}

	/**
	 * Add child node with according key.
	 * 
	 * @param childKey
	 *            Key identifying child node.
	 * @param childNode
	 *            Child node.
	 * @throws PrefixTrieNodeException
	 */
	protected void addChildNode(Character childKey, PrefixTrieNode childNode) throws PrefixTrieNodeException {
		if (this.childs.containsKey(childKey)) {
			throw new PrefixTrieNodeException("Can add multiple childs with same key!");
		}
		this.childs.put(childKey, childNode);
	}

	/**
	 * Check if node has child with according key.
	 * 
	 * @param childKey
	 *            Key identifying child node.
	 * @return If contains key or not.
	 */
	public boolean hasChild(String childKey) {
		return this.childs.containsKey(childKey);
	}

	/**
	 * Get child node with according key.
	 * 
	 * @param childKey
	 *            Key identifying child node.
	 * @return Child node or null.
	 */
	public PrefixTrieNode getChild(String childKey) {
		return this.childs.get(childKey);
	}

	/**
	 * Get child node list.
	 * 
	 * @return Child node list.
	 */
	public HashMap<Character, PrefixTrieNode> getChilds() {
		return this.childs;
	}

	/**
	 * Get all items.
	 * 
	 * @return List of all items.
	 */
	public ArrayList<Character> getAlphabet() {
		return this.alphabet;
	}

	/**
	 * Get node count in tree.
	 * 
	 * @return Node count.
	 */
	public static Integer getNodeCount() {
		return PrefixTrieNode.nodeCount + 1; // +1 for root node
	}

	/**
	 * Reset node count.
	 */
	public static void resetCount() {
		PrefixTrieNode.nodeCount = 0;
	}

	/**
	 * Search for all results matching prefix.
	 * 
	 * @param prefix
	 *            Search prefix.
	 * @return All results matching the prefix.
	 */
	public ArrayList<String> searchAll(String prefix) {
		if (prefix.length() == 0) {
			ArrayList<String> allReturnItems = new ArrayList<String>();
			allReturnItems.addAll(allItems);
			if (this.item != null) {
				allReturnItems.add(item);
			}
			return allReturnItems;
		}
		ArrayList<String> results = new ArrayList<String>();
		Character firstElement = prefix.charAt(0);
		if (this.childs.containsKey(firstElement)) {
			results.addAll(this.childs.get(firstElement).searchAll(prefix.substring(1)));
		}
		return results;
	}

	/**
	 * Search node containing all searched prefix matches.
	 * 
	 * @param prefix
	 *            Search prefix.
	 * @return Node containing all results matching the prefix or null if no
	 *         match found.
	 */
	public PrefixTrieNode searchNode(String prefix) {
		if (prefix.length() == 0) {
			return this;
		}
		Character firstElement = prefix.charAt(0);
		if (this.childs.containsKey(firstElement)) {
			return this.childs.get(firstElement).searchNode(prefix.substring(1));
		}
		return null;
	}

}
