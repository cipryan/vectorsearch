package info.noip.ciprian.vector.trie.exception;

/**
 * Multithreading base exception.
 * 
 * @author Marius Kleiner
 *
 */
@SuppressWarnings("serial")
public class PrefixTrieNodeException extends Exception {

	// Error message
	protected String errorMessage;

	/**
	 * Constructor.
	 * 
	 * @param errorMessage
	 *            Error message text.
	 */
	public PrefixTrieNodeException(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}