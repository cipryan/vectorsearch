package info.noip.ciprian.vector.permutation;

import java.util.ArrayList;

/**
 * Permutation list generator listener.
 * 
 * @author Marius Kleiner
 *
 */
public abstract class PermutationGeneratorListener {

	/**
	 * Get generated permutation result.
	 * 
	 * @param result
	 *            Generated permutation.
	 */
	public abstract void finished(ArrayList<String> result);

}
