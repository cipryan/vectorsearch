package info.noip.ciprian.vector.permutation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Generator serivce for permutations.
 * 
 * @author Marius Kleiner
 *
 */
public class PermutationGenerator {

	// Alphabet of possible item elements
	protected ArrayList<Character> alphabet;
	// Listeners
	protected ArrayList<PermutationGeneratorListener> listeners = new ArrayList<PermutationGeneratorListener>();

	/**
	 * Constructor.
	 * 
	 * @param alphabet
	 *            List of possible characters.
	 */
	public PermutationGenerator(ArrayList<Character> alphabet) {
		this.alphabet = alphabet;
	}

	/**
	 * Add action listener.
	 * 
	 * @param listener
	 *            Listener.
	 */
	public void addListener(PermutationGeneratorListener listener) {
		this.listeners.add(listener);
	}

	/**
	 * Generate an ordered list of all possible permutations.
	 * 
	 * @param size
	 *            Amount of characters.
	 */
	public void generatePermutationList(int size) {
		this.generatePermutationList(size, false);
	}

	/**
	 * Generate list of all possible permutations.
	 * 
	 * @param size
	 *            Amount of characters.
	 * @param randomize
	 *            Get randomized list order.
	 */
	public void generatePermutationList(int size, boolean randomize) {
		ArrayList<String> permutationList = this.recursionOrderedPermutation("", size, 0);
		if (randomize) {
			long seed = System.nanoTime();
			Collections.shuffle(permutationList, new Random(seed));
		}
		for (PermutationGeneratorListener listener : this.listeners) {
			listener.finished(permutationList);
		}
	}

	/**
	 * Recursively generate all possible permutations with given size and
	 * alphabet.
	 * 
	 * @param prefix
	 *            Prefix from last recursion.
	 * @param size
	 *            Maximum size of string.
	 * @param level
	 *            Current recursion level.
	 * @return List of all possible permutations.
	 */
	protected ArrayList<String> recursionOrderedPermutation(String prefix, int size, int level) {
		ArrayList<String> permutationList = new ArrayList<String>();
		if (level + 1 == size) {
			for (Character character : alphabet) {
				permutationList.add(prefix + character.toString());
			}
		} else {
			for (Character character : alphabet) {
				permutationList
						.addAll(this.recursionOrderedPermutation(prefix + character.toString(), size, level + 1));
			}
		}
		return permutationList;
	}

}
