package info.noip.ciprian.vector.search;

import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import info.noip.ciprian.vector.gui.MainFrame;
import info.noip.ciprian.vector.multithreading.exception.InvalidUsageThreadJobException;
import info.noip.ciprian.vector.multithreading.exception.InvalidUsageThreadPoolException;
import info.noip.ciprian.vector.trie.PrefixTrieNode;

/**
 * Vector prefix search.
 * 
 * @author Marius Kleiner
 *
 */
public final class VectorSearch {

	// Permutation list
	private ArrayList<String> permutationList;
	// Prefix tree root node
	private PrefixTrieNode rootNode;

	/**
	 * Main.
	 * 
	 * @throws InvalidUsageThreadJobException
	 * @throws InvalidUsageThreadPoolException
	 * 
	 * @paraArrayList<E>
	 */
	public static void main(String[] args) throws InvalidUsageThreadJobException, InvalidUsageThreadPoolException {

		// Logger
		Logger rootLog = Logger.getLogger(""); rootLog.setLevel(Level.INFO); 
		//rootLog.getHandlers()[0].setLevel(Level.INFO);
		System.setProperty("java.util.logging.SimpleFormatter.format",
				"%1$tY-%1$tm-%1$td %1$tH:%1$tM:%1$tS %4$-6s %5$s%6$s%n");

		// VectorSearch
		VectorSearch vectorSearch = new VectorSearch();
		// MainFrame
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame(vectorSearch);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Set permutations list.
	 * 
	 * @param permutationList
	 */
	public void setPermutationList(ArrayList<String> permutationList) {
		this.permutationList = permutationList;
	}

	/**
	 * Get permutation list.
	 * 
	 * @return Permutation list.
	 */
	public ArrayList<String> getPermutationList() {
		return this.permutationList;
	}

	/**
	 * Set root node.
	 * 
	 * @param rootNode
	 *            Root node.
	 */
	public void setRootNode(PrefixTrieNode rootNode) {
		this.rootNode = rootNode;
	}

	/**
	 * Get root node.
	 * 
	 * @return Root node.
	 */
	public PrefixTrieNode getRootNode() {
		return this.rootNode;
	}

}
