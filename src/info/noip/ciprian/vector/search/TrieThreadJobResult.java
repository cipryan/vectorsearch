package info.noip.ciprian.vector.search;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import info.noip.ciprian.vector.multithreading.ThreadJob;
import info.noip.ciprian.vector.multithreading.ThreadJobResult;
import info.noip.ciprian.vector.multithreading.ThreadPoolJob;
import info.noip.ciprian.vector.multithreading.exception.InvalidUsageThreadJobException;
import info.noip.ciprian.vector.trie.PrefixTrieNode;
import info.noip.ciprian.vector.trie.exception.PrefixTrieNodeException;

/**
 * Result of a trie thread job.
 * 
 * @author Marius Kleiner
 *
 */
public class TrieThreadJobResult extends ThreadJobResult {

	// Trie depth level
	protected Integer level;
	// Parent node of this results child node
	protected PrefixTrieNode parentNode;
	// Map of resulting items matching the according key
	protected HashMap<Character, ArrayList<String>> resultMap;
	// Map of resulting direct item matching the according key
	protected HashMap<Character, String> directMatchMap;

	/**
	 * Constructor.
	 * 
	 * @param level
	 *            Trie depth level.
	 * @param parentNode
	 *            Parent node.
	 * @param resultMap
	 *            Map of resulting items matching the according key.
	 */
	public TrieThreadJobResult(Integer level, PrefixTrieNode parentNode,
			HashMap<Character, ArrayList<String>> resultMap, HashMap<Character, String> directMatchMap) {
		this.level = level;
		this.parentNode = parentNode;
		this.resultMap = resultMap;
		this.directMatchMap = directMatchMap;
	}

	@Override
	public void progressResult(ThreadPoolJob<? extends ThreadJobResult> threadPoolJob)
			throws InvalidUsageThreadJobException {
		if (this.requiresMerge()) {
			this.addToMergeSet(this.getMergeSetId(), threadPoolJob);
			return;
		}
		ArrayList<ThreadJob> childJobs = new ArrayList<ThreadJob>();
		for (Entry<Character, ArrayList<String>> entry : this.resultMap.entrySet()) {
			try {
				// Skip empty
				if (entry.getValue().isEmpty()) {
					continue;
				}
				// Create and append nodes
				PrefixTrieNode childNode;
				if (this.directMatchMap.containsKey(entry.getKey())) {
					childNode = parentNode.appendChild(entry.getKey(), entry.getValue(), this.directMatchMap.get(entry.getKey()));
				} else {
					childNode = parentNode.appendChild(entry.getKey(), entry.getValue());
				}
				// Create a new job for each child node with more than one
				// elements
				if (entry.getValue().size() > 1) {
					TrieThreadJob childJob = new TrieThreadJob(childNode, this.level + 1);
					childJobs.add(childJob);
				}
			} catch (PrefixTrieNodeException e) {
				throw new InvalidUsageThreadJobException(e.getMessage());
			}
		}
		// Add new jobs
		if (!childJobs.isEmpty()) {
			this.addNewJobs(childJobs, threadPoolJob);
		}
		// Put root to final result
		if (this.level == 0) {
			this.addToFinalResults(threadPoolJob);
		}
	}

	@Override
	public ThreadJobResult merge(ThreadJobResult secondMergeResult) throws InvalidUsageThreadJobException {
		TrieThreadJobResult castedSecondMergeResult = this.validate(secondMergeResult);
		// Merge second result into this one and return self
		for (Character character : this.parentNode.getAlphabet()) {
			if (castedSecondMergeResult.getResultMap().get(character).isEmpty()) {
				continue;
			}
			this.getResultMap().get(character).addAll(castedSecondMergeResult.getResultMap().get(character));
		}
		this.getParentNode().getItems().addAll(castedSecondMergeResult.getParentNode().getItems());
		return this;
	}

	/**
	 * Validate merge candidates and cast second merge result.
	 * 
	 * @param secondMergeResult
	 *            Other result instance to merge with.
	 * @return Casted merge instance.
	 * @throws InvalidUsageThreadJobException
	 */
	protected TrieThreadJobResult validate(ThreadJobResult secondMergeResult) throws InvalidUsageThreadJobException {
		if (!(secondMergeResult instanceof TrieThreadJobResult)) {
			throw new InvalidUsageThreadJobException("Impossible to merge " + TrieThreadJobResult.class.getSimpleName()
					+ " with " + secondMergeResult.getClass().getSimpleName() + "!");
		}
		TrieThreadJobResult castedSecondMergeResult = (TrieThreadJobResult) secondMergeResult;
		if (this.level != castedSecondMergeResult.getLevel()) {
			throw new InvalidUsageThreadJobException("Impossible to merge " + TrieThreadJobResult.class.getSimpleName()
					+ " with different depth levels!");
		}
		return castedSecondMergeResult;
	}

	/**
	 * Get level.
	 * 
	 * @return Level.
	 */
	protected Integer getLevel() {
		return this.level;
	}

	/**
	 * Get parent node.
	 * 
	 * @return Parent node.
	 */
	public PrefixTrieNode getParentNode() {
		return this.parentNode;
	}

	/**
	 * Get result map.
	 * 
	 * @return Result map.
	 */
	protected HashMap<Character, ArrayList<String>> getResultMap() {
		return this.resultMap;
	}

}
