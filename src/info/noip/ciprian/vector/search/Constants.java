package info.noip.ciprian.vector.search;

import java.awt.Color;
import java.util.ArrayList;

/**
 * Constants.
 * 
 * @author Marius Kleiner
 *
 */
public abstract class Constants {

	/**
	 * WORDBOOK
	 */
	// ASCII
	public static final Integer ASCII_CAPITAL_A = 65;
	public static final Integer ASCII_CAPITAL_Z = 90;
	// Alphabet
	@SuppressWarnings("serial")
	public static final ArrayList<Character> CAPITAL_A_TO_Z = new ArrayList<Character>() {
		{
			for (int asciiNumber = ASCII_CAPITAL_A; asciiNumber <= ASCII_CAPITAL_Z; asciiNumber++) {
				add((char) asciiNumber);
			}
		}
	};
	// Permutation size
	public static final Integer PERMUTATION_SIZE = 4;

	/**
	 * GUI
	 */
	public static final Integer MAIN_WINDOW_WIDTH = 800;
	public static final Integer MAIN_WINDOW_HEIGHT = 440;
	public static final Integer SEARCH_PANEL_WIDTH = 130;
	public static final Color VECTOR_RED = new Color(183, 0, 50);

}
