package info.noip.ciprian.vector.search;

import java.util.ArrayList;
import java.util.HashMap;

import info.noip.ciprian.vector.multithreading.ThreadJob;
import info.noip.ciprian.vector.multithreading.ThreadJobResult;
import info.noip.ciprian.vector.multithreading.exception.InvalidUsageThreadJobException;
import info.noip.ciprian.vector.trie.PrefixTrieNode;

/**
 * Trie thread job. Searches the given array for matching characters at the
 * corresponding position (depending on trie depth level).
 * 
 * @author Marius Kleiner
 *
 */
public class TrieThreadJob extends ThreadJob {

	// Minimum size for split
	private static final Integer MINIMUM_SIZE_FOR_SPLIT = 1000;
	// Root node for this job
	private PrefixTrieNode node;
	// Trie depht level
	private Integer level;

	/**
	 * Constructor.
	 * 
	 * @param node
	 *            Job root node.
	 * @throws InvalidUsageThreadJobException
	 */
	public TrieThreadJob(PrefixTrieNode node) throws InvalidUsageThreadJobException {
		this(node, 0);
	}

	/**
	 * Constructor.
	 * 
	 * @param node
	 *            Job root node.
	 * @throws InvalidUsageThreadJobException
	 */
	public TrieThreadJob(PrefixTrieNode node, Integer level) throws InvalidUsageThreadJobException {
		super();
		this.level = level;
		this.node = node;
	}

	/**
	 * Constructor.
	 * 
	 * @param node
	 *            Job root node.
	 * @param mergeSetId
	 *            ID of merge set.
	 * @throws InvalidUsageThreadJobException
	 */
	public TrieThreadJob(PrefixTrieNode node, Integer level, Integer mergeSetId)
			throws InvalidUsageThreadJobException {
		super(mergeSetId);
		this.level = level;
		this.node = node;
	}

	@Override
	public ThreadJobResult execute() {
		HashMap<Character, ArrayList<String>> resultMap = this.createAlphabetHashMap();
		HashMap<Character, String> directMatchMap = new HashMap<Character, String>();
		for (String item : this.node.getItems()) {
			Character character = item.charAt(this.level);
			// Check item size
			if (item.length() == this.level) {
				directMatchMap.put(character, item);
			} else {
				resultMap.get(character).add(item);
			}
		}
		ThreadJobResult result = new TrieThreadJobResult(this.level, this.node, resultMap, directMatchMap).setRequiresMerge(this.mergeSetId);
		return result;
	}

	/**
	 * Create a HashMap containing all possible characters as key.
	 * 
	 * @return Alphabet HashMap with empty ArrayLists values.
	 */
	private HashMap<Character, ArrayList<String>> createAlphabetHashMap() {
		HashMap<Character, ArrayList<String>> alphabetHashMap = new HashMap<Character, ArrayList<String>>();
		for (Character character : this.node.getAlphabet()) {
			alphabetHashMap.put(character, new ArrayList<String>());
		}
		return alphabetHashMap;
	}

	@Override
	public int getImportanceValue() {
		return this.node.getItems().size();
	}

	/**
	 * Split this ThreadJob (if possible). Either return self in a list.
	 * 
	 * @param splitCount
	 *            Amount of desired splits.
	 * @return List of job splits.
	 * @throws InvalidUsageThreadJobException 
	 */
	public ArrayList<ThreadJob> split(Integer splitCount) throws InvalidUsageThreadJobException {
		ArrayList<ThreadJob> returnList = new ArrayList<ThreadJob>();
		if (splitCount > 1) {
			ArrayList<ArrayList<String>> chunks = this.splitToChunks(this.node.getItems(), splitCount);
			Integer mergeSetId = this.getNextMergeSetId(chunks.size());
			for (ArrayList<String> chunk : chunks) {
				PrefixTrieNode splitNode = new PrefixTrieNode(
						this.node.getAlphabet(), chunk);
				TrieThreadJob splitNodeJob = new TrieThreadJob(splitNode, this.level, mergeSetId);
				returnList.add(splitNodeJob);
			}
		} else {
			returnList.add(this);
		}
		return returnList;
	}

	/**
	 * Split item list into chunks.
	 * 
	 * @param items
	 *            Item list.
	 * @param chunks
	 *            Amount of desired chunks.
	 * @return Item list chunks.
	 */
	protected ArrayList<ArrayList<String>> splitToChunks(ArrayList<String> items, int chunks) {
		int size = items.size();
		ArrayList<ArrayList<String>> itemChunks = new ArrayList<ArrayList<String>>();
		if (size < TrieThreadJob.MINIMUM_SIZE_FOR_SPLIT) {
			itemChunks.add(items);
			return itemChunks;
		}
		if (size < chunks) {
			chunks = size;
		}
		int chunkSize = size / chunks;
		int rest = size - (chunks * chunkSize);
		int lastIndex = 0;
		for (int i = 0; i < chunks; i++) {
			int toIndex = lastIndex + chunkSize;
			if (i == 0) {
				toIndex += rest;
			}
			itemChunks.add(new ArrayList<String>(items.subList(lastIndex, toIndex)));
			lastIndex = toIndex;
		}
		return itemChunks;
	}

}
