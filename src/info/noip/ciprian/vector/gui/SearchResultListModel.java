package info.noip.ciprian.vector.gui;

import java.util.ArrayList;

import javax.swing.AbstractListModel;

/**
 * Search result list model.
 * 
 * @author Marius Kleiner
 *
 */
@SuppressWarnings("serial")
public class SearchResultListModel extends AbstractListModel<String> {

	// List values
	ArrayList<String> values;

	/**
	 * Constructor.
	 */
	public SearchResultListModel() {
		this(new ArrayList<String>());
	}

	/**
	 * Constructor.
	 * 
	 * @param values List elements
	 */
	public SearchResultListModel(ArrayList<String> values) {
		this.values = values;
	}

	@Override
	public int getSize() {
		return this.values.size();
	}

	@Override
	public String getElementAt(int index) {
		return this.values.get(index);
	}

	/**
	 * Add element to list model.
	 * 
	 * @param element
	 */
	public void addElement(String element) {
		this.values.add(element);
	}

	/**
	 * Replace all elements.
	 * 
	 * @param newElements
	 *            New elements.
	 */
	public void setElements(ArrayList<String> newElements) {
		this.values = newElements;
	}

}
