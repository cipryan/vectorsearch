package info.noip.ciprian.vector.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import info.noip.ciprian.vector.multithreading.ThreadJob;
import info.noip.ciprian.vector.multithreading.ThreadPool;
import info.noip.ciprian.vector.multithreading.ThreadPoolListener;
import info.noip.ciprian.vector.multithreading.exception.InvalidUsageThreadJobException;
import info.noip.ciprian.vector.multithreading.exception.InvalidUsageThreadPoolException;
import info.noip.ciprian.vector.permutation.PermutationGenerator;
import info.noip.ciprian.vector.permutation.PermutationGeneratorListener;
import info.noip.ciprian.vector.search.Constants;
import info.noip.ciprian.vector.search.TrieThreadJob;
import info.noip.ciprian.vector.search.TrieThreadJobResult;
import info.noip.ciprian.vector.search.VectorSearch;
import info.noip.ciprian.vector.trie.PrefixTrieNode;
import java.awt.ComponentOrientation;
import javax.swing.JMenuItem;

@SuppressWarnings("serial")
public class MainFrame extends JFrame {

	// UI Elements
	private JPanel contentPane;
	private JTextField textFieldSizePermutation;
	private JTextField textFieldSearch;
	private JTextField textFieldDurationPermutation;
	private JTextField textFieldPrefixTrieDuration;
	private JTextField textFieldPrefixTrieNodes;
	private JTextField textFieldSearchDuration;
	private JTextField textFieldSearchMatches;
	// Main program instance
	private VectorSearch vectorSearch;
	private JTextField textFieldIncrementalSearch;
	private JTextField textFieldIncrementalSearchDuration;
	private JTextField textFieldIncrementalSearchMatches;

	/**
	 * Create the frame.
	 * 
	 * @param vectorSearch
	 *            Main program instance.
	 */
	public MainFrame(VectorSearch vectorSearch) {
		this.vectorSearch = vectorSearch;
		setResizable(false);
		setTitle("VectorSearch");
		setForeground(SystemColor.window);
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
		setBackground(Color.WHITE);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setBounds((dim.width - Constants.MAIN_WINDOW_WIDTH) / 2, (dim.height - Constants.MAIN_WINDOW_HEIGHT) / 2,
				Constants.MAIN_WINDOW_WIDTH, Constants.MAIN_WINDOW_HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		mntmExit.setIcon(new ImageIcon(MainFrame.class.getResource("/javax/swing/plaf/metal/icons/ocean/error.png")));
		mnFile.add(mntmExit);

		JMenu mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);
		
		JMenuItem mnAbout = new JMenuItem("About");
		mnAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(MainFrame.this, " VectorInformatik\n VectorSearch (Test Client)\n Marius Kleiner\n 21.05.2017", "About", JOptionPane.PLAIN_MESSAGE);
			}
		});
		mnAbout.setIcon(new ImageIcon(MainFrame.class.getResource("/javax/swing/plaf/metal/icons/ocean/info.png")));
		mnHelp.add(mnAbout);

		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		JPanel HeaderPanel = new JPanel();
		HeaderPanel.setPreferredSize(new Dimension(0, 50));
		HeaderPanel.setBackground(Constants.VECTOR_RED);
		contentPane.add(HeaderPanel, BorderLayout.NORTH);

		JLabel VectorLogoPanel = new JLabel();
		VectorLogoPanel.setFocusable(false);
		VectorLogoPanel.setFocusTraversalKeysEnabled(false);
		HeaderPanel.setLayout(null);
		VectorLogoPanel.setBounds(16, 8, 174, 36);
		HeaderPanel.add(VectorLogoPanel);
		ImageIcon imageIcon = new ImageIcon(
				MainFrame.class.getResource("/info/noip/ciprian/vector/gui/resources/Vector_Logo.png"));
		imageIcon = new ImageIcon(imageIcon.getImage().getScaledInstance(VectorLogoPanel.getWidth(),
				VectorLogoPanel.getHeight(), Image.SCALE_SMOOTH));
		VectorLogoPanel.setIcon(imageIcon);

		JTextPane txtpnFallstudiePnd = new JTextPane();
		txtpnFallstudiePnd.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		txtpnFallstudiePnd.setFont(new Font("Arial", Font.BOLD, 17));
		txtpnFallstudiePnd.setFocusable(false);
		txtpnFallstudiePnd.setFocusCycleRoot(false);
		txtpnFallstudiePnd.setFocusTraversalKeysEnabled(false);
		txtpnFallstudiePnd.setEditable(false);
		txtpnFallstudiePnd.setOpaque(false);
		txtpnFallstudiePnd.setText("Fallstudie PND");
		txtpnFallstudiePnd.setBounds(657, 6, 125, 24);
		HeaderPanel.add(txtpnFallstudiePnd);

		JTextPane txtpnMariusKleiner = new JTextPane();
		txtpnMariusKleiner.setOpaque(false);
		txtpnMariusKleiner.setText("Marius Kleiner");
		txtpnMariusKleiner.setFont(new Font("Arial", Font.PLAIN, 13));
		txtpnMariusKleiner.setFocusable(false);
		txtpnMariusKleiner.setFocusTraversalKeysEnabled(false);
		txtpnMariusKleiner.setFocusCycleRoot(false);
		txtpnMariusKleiner.setEditable(false);
		txtpnMariusKleiner.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		txtpnMariusKleiner.setBounds(682, 26, 100, 18);
		HeaderPanel.add(txtpnMariusKleiner);

		JPanel CenterPanel = new JPanel();
		CenterPanel.setBorder(new EmptyBorder(5, 0, 0, 0));
		CenterPanel.setBackground(Color.WHITE);
		contentPane.add(CenterPanel, BorderLayout.CENTER);
		CenterPanel.setLayout(new BorderLayout(0, 0));

		JPanel panelInteraction = new JPanel();
		panelInteraction.setBackground(Color.WHITE);
		panelInteraction.setBorder(new EmptyBorder(0, 5, 0, 0));
		CenterPanel.add(panelInteraction, BorderLayout.CENTER);
		panelInteraction.setLayout(new BorderLayout(0, 0));

		JPanel panelPermutation = new JPanel();
		panelPermutation
				.setBorder(new TitledBorder(null, "Permutation", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelInteraction.add(panelPermutation, BorderLayout.NORTH);
		panelPermutation.setBackground(Color.WHITE);
		panelPermutation.setLayout(new BorderLayout(0, 0));

		JPanel panelPermutationContent = new JPanel();
		panelPermutationContent.setBorder(new EmptyBorder(5, 5, 5, 5));
		panelPermutationContent.setBackground(Color.WHITE);
		FlowLayout flowLayout = (FlowLayout) panelPermutationContent.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		panelPermutation.add(panelPermutationContent);

		JButton btnGeneratePermutation = new JButton("Generate Permutation");
		panelPermutationContent.add(btnGeneratePermutation);
		btnGeneratePermutation.setHorizontalAlignment(SwingConstants.LEFT);

		JProgressBar progressBarPermutation = new JProgressBar();
		progressBarPermutation.setFocusable(false);
		progressBarPermutation.setFocusTraversalKeysEnabled(false);
		progressBarPermutation.setMaximumSize(new Dimension(100, 14));
		progressBarPermutation.setRequestFocusEnabled(false);
		progressBarPermutation.setBackground(Color.WHITE);
		progressBarPermutation.setPreferredSize(new Dimension(100, 25));
		panelPermutationContent.add(progressBarPermutation);

		JPanel panelPermutationResult = new JPanel();
		panelPermutationResult.setBackground(Color.WHITE);
		panelPermutationResult.setBorder(new EmptyBorder(0, 20, 0, 0));
		panelPermutationContent.add(panelPermutationResult);

		JTextPane txtpnDurationms = new JTextPane();
		txtpnDurationms.setFocusCycleRoot(false);
		txtpnDurationms.setFocusTraversalKeysEnabled(false);
		txtpnDurationms.setFocusable(false);
		txtpnDurationms.setRequestFocusEnabled(false);
		txtpnDurationms.setEditable(false);
		panelPermutationResult.add(txtpnDurationms);
		txtpnDurationms.setText("Duration (ms):");

		textFieldDurationPermutation = new JTextField();
		panelPermutationResult.add(textFieldDurationPermutation);
		textFieldDurationPermutation.setPreferredSize(new Dimension(25, 24));
		textFieldDurationPermutation.setMinimumSize(new Dimension(25, 25));
		textFieldDurationPermutation.setFocusable(false);
		textFieldDurationPermutation.setEditable(false);
		textFieldDurationPermutation.setColumns(5);

		JTextPane txtpnSize = new JTextPane();
		txtpnSize.setFocusable(false);
		txtpnSize.setFocusTraversalKeysEnabled(false);
		txtpnSize.setFocusCycleRoot(false);
		txtpnSize.setEditable(false);
		panelPermutationResult.add(txtpnSize);
		txtpnSize.setText("Size:");

		textFieldSizePermutation = new JTextField();
		panelPermutationResult.add(textFieldSizePermutation);
		textFieldSizePermutation.setEditable(false);
		textFieldSizePermutation.setPreferredSize(new Dimension(25, 24));
		textFieldSizePermutation.setFocusable(false);
		textFieldSizePermutation.setMinimumSize(new Dimension(25, 25));
		textFieldSizePermutation.setColumns(10);

		JPanel panelCenterInteraction = new JPanel();
		panelCenterInteraction.setBorder(new EmptyBorder(0, 0, 0, 0));
		panelInteraction.add(panelCenterInteraction, BorderLayout.CENTER);
		panelCenterInteraction.setLayout(new BorderLayout(0, 0));

		JPanel panelPrefixTrie = new JPanel();
		panelCenterInteraction.add(panelPrefixTrie, BorderLayout.NORTH);
		panelPrefixTrie.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "PrefixTrie",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelPrefixTrie.setBackground(Color.WHITE);
		panelPrefixTrie.setLayout(new BorderLayout(0, 0));

		JPanel panelPrefixTrieContent = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) panelPrefixTrieContent.getLayout();
		flowLayout_1.setAlignment(FlowLayout.LEFT);
		panelPrefixTrieContent.setBorder(new EmptyBorder(5, 5, 5, 5));
		panelPrefixTrieContent.setBackground(Color.WHITE);
		panelPrefixTrie.add(panelPrefixTrieContent, BorderLayout.NORTH);

		JButton btnBuildPrefixTrie = new JButton("Build PrefixTrie");
		btnBuildPrefixTrie.setEnabled(false);
		btnBuildPrefixTrie.setHorizontalAlignment(SwingConstants.LEFT);
		panelPrefixTrieContent.add(btnBuildPrefixTrie);

		JPanel panelPrefixTrieProgressBarContainer = new JPanel();
		panelPrefixTrieProgressBarContainer.setBackground(Color.WHITE);
		panelPrefixTrieProgressBarContainer.setBorder(new EmptyBorder(0, 38, 0, 0));
		panelPrefixTrieContent.add(panelPrefixTrieProgressBarContainer);
		panelPrefixTrieProgressBarContainer.setLayout(new BorderLayout(0, 0));

		JProgressBar progressBarPrefixTrie = new JProgressBar();
		progressBarPrefixTrie.setFocusable(false);
		progressBarPrefixTrie.setFocusTraversalKeysEnabled(false);
		panelPrefixTrieProgressBarContainer.add(progressBarPrefixTrie);
		progressBarPrefixTrie.setRequestFocusEnabled(false);
		progressBarPrefixTrie.setPreferredSize(new Dimension(100, 25));
		progressBarPrefixTrie.setBackground(Color.WHITE);

		JPanel panelPrefixTrieResult = new JPanel();
		panelPrefixTrieResult.setBorder(new EmptyBorder(0, 20, 0, 0));
		panelPrefixTrieResult.setBackground(Color.WHITE);
		panelPrefixTrieContent.add(panelPrefixTrieResult);

		JTextPane txtpnDurationms_1 = new JTextPane();
		txtpnDurationms_1.setFocusable(false);
		txtpnDurationms_1.setFocusTraversalKeysEnabled(false);
		txtpnDurationms_1.setFocusCycleRoot(false);
		txtpnDurationms_1.setEditable(false);
		txtpnDurationms_1.setText("Duration (ms):");
		panelPrefixTrieResult.add(txtpnDurationms_1);

		textFieldPrefixTrieDuration = new JTextField();
		textFieldPrefixTrieDuration.setPreferredSize(new Dimension(25, 24));
		textFieldPrefixTrieDuration.setMinimumSize(new Dimension(25, 25));
		textFieldPrefixTrieDuration.setFocusable(false);
		textFieldPrefixTrieDuration.setEditable(false);
		textFieldPrefixTrieDuration.setColumns(5);
		panelPrefixTrieResult.add(textFieldPrefixTrieDuration);

		JTextPane txtpnNodes = new JTextPane();
		txtpnNodes.setFocusable(false);
		txtpnNodes.setFocusTraversalKeysEnabled(false);
		txtpnNodes.setFocusCycleRoot(false);
		txtpnNodes.setEditable(false);
		txtpnNodes.setText("Nodes:");
		panelPrefixTrieResult.add(txtpnNodes);

		textFieldPrefixTrieNodes = new JTextField();
		textFieldPrefixTrieNodes.setPreferredSize(new Dimension(25, 24));
		textFieldPrefixTrieNodes.setMinimumSize(new Dimension(25, 25));
		textFieldPrefixTrieNodes.setFocusable(false);
		textFieldPrefixTrieNodes.setEditable(false);
		textFieldPrefixTrieNodes.setColumns(9);
		panelPrefixTrieResult.add(textFieldPrefixTrieNodes);

		JPanel panelSearch = new JPanel();
		panelCenterInteraction.add(panelSearch);
		panelSearch.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Search",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelSearch.setBackground(Color.WHITE);
		panelSearch.setLayout(new BorderLayout(0, 0));

		JPanel panelSearchContent = new JPanel();
		FlowLayout fl_panelSearchContent = (FlowLayout) panelSearchContent.getLayout();
		fl_panelSearchContent.setAlignment(FlowLayout.LEFT);
		panelSearchContent.setBorder(new EmptyBorder(5, 5, 5, 5));
		panelSearchContent.setBackground(Color.WHITE);
		panelSearch.add(panelSearchContent, BorderLayout.NORTH);

		textFieldSearch = new JTextField();
		textFieldSearch.setEnabled(false);
		textFieldSearch.setPreferredSize(new Dimension(25, 24));
		textFieldSearch.setMinimumSize(new Dimension(25, 25));
		textFieldSearch.setColumns(16);
		panelSearchContent.add(textFieldSearch);

		JButton btnSearch = new JButton("Search");
		btnSearch.setEnabled(false);
		btnSearch.setHorizontalAlignment(SwingConstants.LEFT);
		panelSearchContent.add(btnSearch);

		JPanel panelSearchResult = new JPanel();
		panelSearchResult.setBorder(new EmptyBorder(0, 22, 0, 0));
		panelSearchResult.setBackground(Color.WHITE);
		panelSearchContent.add(panelSearchResult);

		JTextPane txtpnDurationms_2 = new JTextPane();
		txtpnDurationms_2.setFocusable(false);
		txtpnDurationms_2.setFocusTraversalKeysEnabled(false);
		txtpnDurationms_2.setFocusCycleRoot(false);
		txtpnDurationms_2.setEditable(false);
		txtpnDurationms_2.setText("Duration (ms):");
		panelSearchResult.add(txtpnDurationms_2);

		textFieldSearchDuration = new JTextField();
		textFieldSearchDuration.setPreferredSize(new Dimension(25, 24));
		textFieldSearchDuration.setMinimumSize(new Dimension(25, 25));
		textFieldSearchDuration.setFocusable(false);
		textFieldSearchDuration.setEditable(false);
		textFieldSearchDuration.setColumns(5);
		panelSearchResult.add(textFieldSearchDuration);

		JTextPane txtpnMatches = new JTextPane();
		txtpnMatches.setFocusable(false);
		txtpnMatches.setFocusTraversalKeysEnabled(false);
		txtpnMatches.setFocusCycleRoot(false);
		txtpnMatches.setEditable(false);
		txtpnMatches.setText("Matches:");
		panelSearchResult.add(txtpnMatches);

		textFieldSearchMatches = new JTextField();
		textFieldSearchMatches.setPreferredSize(new Dimension(25, 24));
		textFieldSearchMatches.setMinimumSize(new Dimension(25, 25));
		textFieldSearchMatches.setFocusable(false);
		textFieldSearchMatches.setEditable(false);
		textFieldSearchMatches.setColumns(8);
		panelSearchResult.add(textFieldSearchMatches);

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "IncrementalSearch",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel.setBackground(Color.WHITE);
		panelCenterInteraction.add(panel, BorderLayout.SOUTH);
		panel.setLayout(new BorderLayout(0, 0));

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new EmptyBorder(5, 0, 5, 10));
		panel_1.setBackground(Color.WHITE);
		panel.add(panel_1, BorderLayout.NORTH);

		textFieldIncrementalSearch = new JTextField();
		textFieldIncrementalSearch.setPreferredSize(new Dimension(25, 24));
		textFieldIncrementalSearch.setMinimumSize(new Dimension(25, 25));
		textFieldIncrementalSearch.setEnabled(false);
		textFieldIncrementalSearch.setColumns(16);
		panel_1.add(textFieldIncrementalSearch);

		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new EmptyBorder(0, 100, 0, 0));
		panel_2.setBackground(Color.WHITE);
		panel_1.add(panel_2);

		JTextPane textPane = new JTextPane();
		textPane.setText("Duration (ms):");
		textPane.setFocusable(false);
		textPane.setFocusTraversalKeysEnabled(false);
		textPane.setFocusCycleRoot(false);
		textPane.setEditable(false);
		panel_2.add(textPane);

		textFieldIncrementalSearchDuration = new JTextField();
		textFieldIncrementalSearchDuration.setPreferredSize(new Dimension(25, 24));
		textFieldIncrementalSearchDuration.setMinimumSize(new Dimension(25, 25));
		textFieldIncrementalSearchDuration.setFocusable(false);
		textFieldIncrementalSearchDuration.setEditable(false);
		textFieldIncrementalSearchDuration.setColumns(5);
		panel_2.add(textFieldIncrementalSearchDuration);

		JTextPane textPane_1 = new JTextPane();
		textPane_1.setText("Matches:");
		textPane_1.setFocusable(false);
		textPane_1.setFocusTraversalKeysEnabled(false);
		textPane_1.setFocusCycleRoot(false);
		textPane_1.setEditable(false);
		panel_2.add(textPane_1);

		textFieldIncrementalSearchMatches = new JTextField();
		textFieldIncrementalSearchMatches.setPreferredSize(new Dimension(25, 24));
		textFieldIncrementalSearchMatches.setMinimumSize(new Dimension(25, 25));
		textFieldIncrementalSearchMatches.setFocusable(false);
		textFieldIncrementalSearchMatches.setEditable(false);
		textFieldIncrementalSearchMatches.setColumns(8);
		panel_2.add(textFieldIncrementalSearchMatches);

		JPanel panelSearchResults = new JPanel();
		panelSearchResults.setBorder(new EmptyBorder(0, 5, 0, 5));
		panelSearchResults.setPreferredSize(new Dimension(Constants.SEARCH_PANEL_WIDTH, 0));
		CenterPanel.add(panelSearchResults, BorderLayout.EAST);
		panelSearchResults.setBackground(Color.WHITE);
		panelSearchResults.setLayout(new BorderLayout(0, 0));

		JPanel panelSearchResultContent = new JPanel();
		panelSearchResults.add(panelSearchResultContent, BorderLayout.CENTER);
		panelSearchResultContent
				.setBorder(new TitledBorder(null, "Result", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelSearchResultContent.setBackground(Color.WHITE);
		panelSearchResultContent.setLayout(new BorderLayout(0, 0));

		SearchResultListModel listModel = new SearchResultListModel();

		JList<String> list = new JList<String>();
		list.setBorder(new EmptyBorder(10, 10, 10, 10));
		list.setFont(new Font("Arial", Font.PLAIN, 15));
		list.setModel(listModel);
		list.setVisibleRowCount(-1);

		JScrollPane scrollPane = new JScrollPane(list);
		scrollPane.setBorder(new EmptyBorder(0, 0, 0, 0));
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		panelSearchResultContent.add(scrollPane, BorderLayout.CENTER);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBackground(Color.WHITE);

		JPanel panelBottom = new JPanel();
		panelBottom.setBackground(Color.WHITE);
		panelBottom.setBorder(new EmptyBorder(5, 0, 0, 0));
		CenterPanel.add(panelBottom, BorderLayout.SOUTH);
		panelBottom.setLayout(new BorderLayout(0, 0));

		// Permutation action listener
		btnGeneratePermutation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent action) {
				btnGeneratePermutation.setEnabled(false);
				progressBarPermutation.setIndeterminate(true);
				Logger.getLogger(VectorSearch.class.getName()).log(Level.INFO, VectorSearch.class.getSimpleName()
						+ ": Generating permutation of size " + Constants.PERMUTATION_SIZE);
				final Long permutationStart = System.currentTimeMillis();
				PermutationGenerator pg = new PermutationGenerator(Constants.CAPITAL_A_TO_Z);
				pg.addListener(new PermutationGeneratorListener() {

					@Override
					public void finished(ArrayList<String> result) {
						vectorSearch.setPermutationList(result);
						progressBarPermutation.setIndeterminate(false);
						progressBarPermutation.setValue(100);
						textFieldSizePermutation.setText(new Integer(result.size()).toString());
						Long permutationDuration = System.currentTimeMillis() - permutationStart;
						textFieldDurationPermutation.setText(permutationDuration.toString());
						Logger.getLogger(VectorSearch.class.getName()).log(Level.INFO,
								VectorSearch.class.getSimpleName() + ": Generated permutation of size "
										+ Constants.PERMUTATION_SIZE + " with " + result.size() + " elements ("
										+ permutationDuration + " ms)");
						btnBuildPrefixTrie.setEnabled(true);

					}

				});
				pg.generatePermutationList(Constants.PERMUTATION_SIZE, true);
			}
		});

		// PrefixTrie action listener
		btnBuildPrefixTrie.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent action) {
				btnSearch.setEnabled(false);
				textFieldIncrementalSearch.setEnabled(false);
				textFieldSearch.setEnabled(false);
				btnBuildPrefixTrie.setEnabled(false);
				progressBarPrefixTrie.setIndeterminate(true);
				// PrefixTrie
				ArrayList<String> permutationList = MainFrame.this.vectorSearch.getPermutationList();
				PrefixTrieNode root = new PrefixTrieNode(Constants.CAPITAL_A_TO_Z, permutationList);
				PrefixTrieNode.resetCount();
				ThreadPool<TrieThreadJobResult> threadPool;
				try {
					TrieThreadJob initialJob = new TrieThreadJob(root);
					// Job list
					ArrayList<ThreadJob> initialTrieJobList = new ArrayList<ThreadJob>();
					initialTrieJobList.add(initialJob);
					// Thread pool
					threadPool = new ThreadPool<TrieThreadJobResult>(initialTrieJobList);
					threadPool.addThreadPoolListener(new ThreadPoolListener() {

						// Sort duration
						private Long sortDuration;

						@Override
						public void notifyStarted() {
							Logger.getLogger(VectorSearch.class.getName()).log(Level.INFO,
									VectorSearch.class.getSimpleName() + ": Start creating trie from "
											+ permutationList.size() + " items");
							this.sortDuration = System.currentTimeMillis();
						}

						@Override
						public void notifyFinished() {
							this.sortDuration = System.currentTimeMillis() - this.sortDuration;
							Logger.getLogger(VectorSearch.class.getName()).log(Level.INFO,
									VectorSearch.class.getSimpleName() + ": Finished creating trie with "
											+ PrefixTrieNode.getNodeCount() + " nodes (" + this.sortDuration + " ms)");
							progressBarPrefixTrie.setIndeterminate(false);
							progressBarPrefixTrie.setValue(100);
							textFieldPrefixTrieDuration.setText(this.sortDuration.toString());
							textFieldPrefixTrieNodes.setText(PrefixTrieNode.getNodeCount().toString());
							try {
								ArrayList<TrieThreadJobResult> results = threadPool.getResults();
								TrieThreadJobResult result = results.get(0);
								PrefixTrieNode rootNode = result.getParentNode();
								btnSearch.setEnabled(true);
								textFieldIncrementalSearch.setEnabled(true);
								textFieldSearch.setEnabled(true);
								MainFrame.this.vectorSearch.setRootNode(rootNode);
								ArrayList<String> allResults = new ArrayList<String>();
								if (rootNode != null) {
									allResults.addAll(rootNode.getItems());
									if (rootNode.hasDirectMatchItem()) {
										allResults.add(rootNode.getDirectMatchItem());
									}
								}
								SearchResultListModel listModel = new SearchResultListModel(allResults);
								list.setModel(listModel);
							} catch (InvalidUsageThreadPoolException e) {
								JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
							}
						}

						@Override
						public void notifyThreadFailure(int threadNumber, Exception e) {
							Logger.getLogger(VectorSearch.class.getName()).log(Level.INFO,
									VectorSearch.class.getSimpleName() + ": Thread # " + threadNumber + " failed");
						}

						@Override
						public void notifyJobFailure() {
							Logger.getLogger(VectorSearch.class.getName()).log(Level.INFO,
									VectorSearch.class.getSimpleName() + ": Job failed");
							JOptionPane.showMessageDialog(null, "Building PrefixTrie failed", "Error",
									JOptionPane.ERROR_MESSAGE);
						}

					});
					threadPool.execute();
				} catch (InvalidUsageThreadJobException | InvalidUsageThreadPoolException e) {
					JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					btnBuildPrefixTrie.setEnabled(false);
				}
			}
		});

		// Search action listener
		btnSearch.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String prefix = textFieldSearch.getText();
				PrefixTrieNode rootNode = MainFrame.this.vectorSearch.getRootNode();
				Logger.getLogger(VectorSearch.class.getName()).log(Level.INFO, VectorSearch.class.getSimpleName()
						+ ": Start searching results for prefix results '" + prefix + "'");
				Long searchDuration = System.currentTimeMillis();
				PrefixTrieNode node = rootNode.searchNode(prefix);
				searchDuration = System.currentTimeMillis() - searchDuration;
				ArrayList<String> allResults = new ArrayList<String>();
				if (node != null) {
					allResults.addAll(node.getItems());
					if (node.hasDirectMatchItem()) {
						allResults.add(node.getDirectMatchItem());
					}
				}
				Logger.getLogger(VectorSearch.class.getName()).log(Level.INFO,
						VectorSearch.class.getSimpleName() + ": Finished searching results for prefix results '"
								+ prefix + "': " + allResults.size() + " (" + searchDuration + " ms)");
				textFieldSearchMatches.setText(new Integer(allResults.size()).toString());
				textFieldSearchDuration.setText(searchDuration.toString());
				SearchResultListModel listModel = new SearchResultListModel(allResults);
				list.setModel(listModel);
			}

		});

		// Incremental search action listener
		textFieldIncrementalSearch.getDocument().addDocumentListener(new DocumentListener() {

			// Last search node
			PrefixTrieNode lastNode = null;
			// Last prefix
			String lastPrefix = "";
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				changeEvent();
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				changeEvent();
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				changeEvent();
			}

			/**
			 * Trigger search on change
			 */
			public void changeEvent() {
				String prefix = textFieldIncrementalSearch.getText();
				PrefixTrieNode rootNode = MainFrame.this.vectorSearch.getRootNode();
				if (this.lastNode != null) {
					if (prefix.startsWith(this.lastPrefix)) {
						rootNode = this.lastNode;
						int lastPrefixSize = lastPrefix.length();
						prefix = prefix.substring(lastPrefixSize);
					}
				}
				Logger.getLogger(VectorSearch.class.getName()).log(Level.INFO, VectorSearch.class.getSimpleName()
						+ ": Start searching results for prefix results '" + prefix + "'");
				Long searchDuration = System.currentTimeMillis();
				PrefixTrieNode node = rootNode.searchNode(prefix);
				searchDuration = System.currentTimeMillis() - searchDuration;
				ArrayList<String> allResults = new ArrayList<String>();
				if (node != null) {
					allResults.addAll(node.getItems());
					if (node.hasDirectMatchItem()) {
						allResults.add(node.getDirectMatchItem());
					}
				}
				this.lastNode = node;
				this.lastPrefix = prefix;
				Logger.getLogger(VectorSearch.class.getName()).log(Level.INFO,
						VectorSearch.class.getSimpleName() + ": Finished searching results for prefix results '"
								+ prefix + "': " + allResults.size() + " (" + searchDuration + " ms)");
				textFieldIncrementalSearchMatches.setText(new Integer(allResults.size()).toString());
				textFieldIncrementalSearchDuration.setText(searchDuration.toString());
				SearchResultListModel listModel = new SearchResultListModel(allResults);
				list.setModel(listModel);
			}
		});
	}
}
